FROM nimaebrazi/php-8.1-fpm

LABEL maintainer="Nima Ebrazi nimaebrazi1373@gmail.com"


RUN apt-get update &&  apt-get install -y \ 
        git \
        nginx \
        wget \ 
    	ca-certificates \
        python3-pip \       
        python3 \
        zip \
    	unzip; 


RUN pip install supervisor supervisor-stdout


# Supervisor config
RUN rm /usr/local/etc/php-fpm.d/www.conf
COPY ./deployment/www.conf /usr/local/etc/php-fpm.d/www.conf

COPY ./deployment/supervisord.conf /etc/supervisord.conf

# Override nginx's default config
RUN rm /etc/nginx/sites-enabled/default
COPY ./deployment/default.conf /etc/nginx/sites-enabled/default.conf


# Copy Scripts
COPY ./deployment/start.sh /start.sh
RUN chmod +x /start.sh


# Configure non-root user.
ARG PUID=1000
ENV PUID ${PUID}
ARG PGID=1000
ENV PGID ${PGID}

RUN groupmod --non-unique --gid ${PGID} www-data && \
    usermod --non-unique --uid ${PUID} --gid www-data www-data


WORKDIR /var/www

COPY . .
RUN touch storage/logs/laravel.log
COPY .env.example ./.env
RUN chown -R www-data:www-data .

RUN composer install --ignore-platform-reqs --no-interaction --prefer-source --optimize-autoloader --no-dev --quiet
#RUN composer install --ignore-platform-reqs --no-interaction --prefer-source --optimize-autoloader --no-dev

# forward request and error logs to docker log collector
RUN mkdir -p /var/log/nginx && \
    touch /var/log/nginx/access.log && \
    touch /var/log/nginx/error.log && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

EXPOSE 80

CMD ["/start.sh"]

